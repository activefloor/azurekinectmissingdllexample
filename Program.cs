﻿using System;

namespace AzureKinectMissingExample
{
    class Program
    {
        static void Main(string[] args)
        {
            Microsoft.Azure.Kinect.Sensor.Device.GetInstalledCount();

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
